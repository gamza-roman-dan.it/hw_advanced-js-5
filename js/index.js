"use strict"

const users = "https://ajax.test-danit.com/api/json/users";
const posts = "https://ajax.test-danit.com/api/json/posts";

class Card {
    constructor(id, userName, name, email, posts) {
        this.id = id;
        this.userName = userName;
        this.name = name;
        this.email = email;
        this.posts = posts;
    }

    div = document.createElement("div");

    render(){
        this.div.className = "box";
        this.div.dataset.id = this.id;
        this.div.insertAdjacentHTML("beforeend", `
              <h3 class="user-name">${this.userName}</h3>
              <p class="name">${this.name}</p>
              <p class="email">${this.email}</p>
        `);

        this.posts.forEach(({id, title, body}) => {

           const content = document.createElement("div");
           const button = document.createElement("button");


            content.className = "container";
            content.insertAdjacentHTML("beforeend", `
             <h1 class="title">${title}</h1>
             <p class="content">${body}</p>
        `)
            button.innerText = "delete";
            content.append(button);
            this.div.append(content);

            button.addEventListener("click", () => {
                axios.delete(`https://ajax.test-danit.com/api/json/posts/${id}`)
                    .then(({status}) => {
                        if(status === 200) {
                            content.remove()
                        }
                    })
                    .catch(err => console.log(err));
            });
        })

        document.body.append(this.div);
    };

}

axios.get(users)
    .then(({data}) => {
        data.forEach(({id, username, name, email}) => {

            axios.get(posts).then(({data}) => {
                let posts = [];

                data.forEach((el) => {
                    if(el.userId === id){
                         posts.push(el)
                    }
                });

                new Card(id,username, name, email, posts).render();
            })
                .catch(err => console.log(err));

        })
    })
    .catch(err => console.log(err));



